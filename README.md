﻿# About AVS Sample

This is an AVS (Alexa Voice Service) client sample app on windows.

It is directly based on, and copied from, an Alexa repo [alexa-avs-sample-app ](https://github.com/alexa/alexa-avs-sample-app).


This sample Java client uses a Node.js server for authentication.
(The source code for Android and iOS mobile apps from the original repo are also included, but not used.)
 

* [Get started - Windows](https://github.com/alexa/alexa-avs-sample-app/wiki/Windows)
* [Create Security Profile](https://github.com/alexa/alexa-avs-sample-app/wiki/Create-Security-Profile)
* [Login with Amazon](https://developer.amazon.com/public/apis/engage/login-with-amazon/login-with-amazon)



